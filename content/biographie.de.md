+++
title = 'Biografie'
slug = 'biografie'
image = 'images/kindheit.jpg'
description = 'Wanda Wiłkomirska, die bedeutendste und bekannteste polnische Geigerin, entstammt einer angesehenen Musikerfamilie. Hier Ihr erfahrt mehr zu ihrer Biografie'
disableComments = true
+++
Wanda Wiłkomirska, die bedeutendste und bekannteste polnische Geigerin, entstammt einer angesehenen Musikerfamilie. Den ersten Violinunterricht erhielt sie von ihrem Vater. Das Musikstudium schloss sie mit einem Diplom der Staatlichen Musikhochschule in Lodz in der Klasse von Prof. Irena Dubiska (1947) und der Ferenc Liszt-Akademie in Budapest in der Klasse von Ede Zathureczki (1950) ab. Um ihr Violinspiel zu vervollkommnen, setzte sie nach dem Studium ihre Ausbildung in Paris bei Henryk Szeryng und in Warschau bei Tadeusz Wroński fort; letzterer bereitete sie auf den Henryk Wieniawski-Wettbewerb in Posen vor.

Die Kammermusik nimmt einen sehr wichtigen Platz in der musikalischen Tätigkeit der Geigerin ein, die in jungen Jahren mit ihrer Schwester Maria am Klavier und ihrem Bruder Kazimierz am Violoncello als "Trio Wilkomirski" auftrat. Als Kammermusikerin arbeitete sie auch mit so bekannten Musikern wie Krystian Zimerman, Gidon Kremer, Martha Argerich, Kim Kashkashin und  Mischa Maisky zusammen. Sie trat u. a. bei den Festivals „Bravo Maestro“, "Gidon Kremer & Friends" in Kumho und "Martha Argerich & Friends" in Bochum auf. Mehrfach wurde die Künstlerin zur Teilnahme an Musikfestivals in Edinburg, Wien, Salzburg, Paris und Warschau eingeladen.
