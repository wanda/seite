+++
title = 'Biography'
slug = 'biography'
image = 'images/kindheit.jpg'
description ='Wanda Wiłkomirska, the most important and famous Polish violinist, comes from a distinguished family of musicians. Here you can learn more about her biography.' 
disableComments = true
+++
Wanda Wiłkomirska, the most important and best-known Polish violinist, comes from a distinguished family of musicians. She received her first violin lessons from her father. She graduated from the State Academy of Music in Lodz in the class of Prof. Irena Dubiska (1947) and from the Ferenc Liszt Academy in Budapest in the class of Ede Zathureczki (1950). In order to perfect her violin playing, after graduation she continued her studies in Paris with Henryk Szeryng and in Warsaw with Tadeusz Wroński; the latter prepared her for the Henryk Wieniawski Competition in Poznan.

Chamber music occupies a very important place in the musical activity of the violinist, who in her younger years performed with her sister Maria on piano and her brother Kazimierz on cello as the "Trio Wilkomirski". As a chamber musician, she also collaborated with such well-known musicians as Krystian Zimerman, Gidon Kremer, Martha Argerich, Kim Kashkashin and Mischa Maisky. She has performed at festivals such as "Bravo Maestro", "Gidon Kremer & Friends" in Kumho and "Martha Argerich & Friends" in Bochum. Several times the artist was invited to participate in music festivals in Edinburg, Vienna, Salzburg, Paris and Warsaw.
