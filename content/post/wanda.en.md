+++
title = 'Whho is Wanda Wilkomirska'
slug = 'wer-ist-wanda'
image = 'images/wanda.jpg'
description = "This page is dedicated to the great Polish violinist Wanda Wiłkomirska. Her musical career led straight from success to success, up to the Olympus of the world's best performers. Her private life, on the other hand, rich in political entanglements and liberation blows"
disableComments = true
+++
The great Polish violinist Wanda Wiłkomirska, who has died at the age of 89, was particularly associated with the repertoire of her compatriots Karol Szymanowski, Krzysztof Penderecki - who wrote his Capriccio for her in 1967 - and Grażyna Bacewicz. She was also a champion of other 20th-century repertoire, especially Béla Bartók and Paul Hindemith, and felt a connection with British composers Frederick Delius, whom she considered an English Szymanowski who recorded his three violin sonatas, and Benjamin Britten.

We invite you on a journey through the life of Wanda. Focal points are:

- Childhood
- student
- World War II
- Carnergie Hall
- Career
- Husband
- Opposition 1976

W. Wiłkomirska is a laureate of violin competitions in Geneva (1946), Budapest (1949), Leipzig (1950) and Poznan (1952). Successful participation in these competitions, as well as performances with the Warsaw Philharmonic Orchestra, made the artist well-known in Poland and abroad. Of particular importance for her career was the musical connection with the conductor Witold Rowicki, who had a decisive influence on the Warsaw National Philharmonic Orchestra. In 1955 Wanda Wiłkomirska was appointed soloist of the orchestra. Together with Rowicki, she performed on many stages around the world. A turning point in her career was her appearance at New York's Carnegie Hall, where in 1961 she performed Karol Szymanowski's Violin Concerto No. 1 together with the National Philharmonic Orchestra. Sol Hurock, at that time the most famous music impresario, who looked after such renowned violinists as I. Stern and D. Oistrakh, noticed W. Wiłkomirska's extraordinary talent and opened the doors to the American and Canadian music market for her. From then on, she gave annual concerts in America as part of violin recitals and philharmonic concerts. In 1968 she began a permanent collaboration with the Connoisseur Society record company in New York, for which she recorded 12 records, two of which received the "Best of the Year" award (1972) and the "Grand Prix du Disque"(1974). Since that time the violinist has cooperated with the biggest record companies such as Deutsche Grammophon, EMI, Philips, Naxos and Polskie Nagrania.

In 1969, W. Wiłkomirska achieved great artistic success in Australia. In 1973 she appeared as the first soloist at the Sydney Opera. Her playing won her great recognition; the violinist was invited to numerous violin recitals and concerts with Australian symphony orchestras. Since the early 1960s she has performed in the world's most prestigious concert halls, such as Carnegie Hall, Lincoln Center New York, Salle Pleyel in Paris, Gewandhaus zu Leipzig, Royal Festival Hall, Great Hall of Tchaikovsky Conservatory Moscow, Berlin Philharmonic and Sydney Opera. Wanda Wilkomirska has performed with the following orchestras, among others: New York Philharmonic, Cleveland Orchestra, Halle Orchestra, Royal Philharmonic, Sydney Symphony, Leipzig Gewandhaus Orchestra, Scottish Chamber Orchestra, Royal Concertgebouw Orchestra, Berlin Philharmonic. She played under the baton of such famous conductors as Paul Klecki, Pierre Boulez, Paul Hindemith, Otto Klemperer, Zubin Mehta, Sir John Barbirolli, Wolfgang Sawallisch, Kurt Masur and Erich Leinsdorf. In 1976, under Leinsdorf's direction, she opened the world-famous Barbican Hall in London with a performance of B. Britten's Violin Concerto. In 1986, after institutional apartheid had been abolished in South Africa, she accepted an invitation to perform there for the first time. Since then, Wanda Wilkomirska can be said to be known on all five continents. Despite these successes, she continued her cooperation with smaller Polish orchestras and philharmonies, which enabled her to gain new fans among music lovers and in music circles.

Chamber music occupies a very important place in the musical activity of the violinist, who in her younger years performed with her sister Maria on piano and her brother Kazimierz on cello as the "Trio Wilkomirski". As a chamber musician, she also collaborated with such well-known musicians as Krystian Zimerman, Gidon Kremer, Martha Argerich, Kim Kashkashin and Mischa Maisky. She performed at the festivals "Bravo Maestro", "Gidon Kremer & Friends" in Kumho and "Martha Argerich & Friends" in Bochum, among others. Several times the artist was invited to participate in music festivals in Edinburg, Vienna, Salzburg, Paris and Warsaw.

In her career, world premieres of Polish contemporary musical works play a major role. Among the most important premieres are works such as Violin Concerto no. 5 (1951) and Violin Concerto no. 7 (1979) by Grażyna Bacewicz, the Espressioni Varianti by Tadeusz Baird (1959), Dialoghi per violino e orchestra by Augustyn Bloch (1966), the Capriccio by Krzysztof Penderecki (1968), the Violin Concerto by Zbigniew Bargielski (1977), the Violin Concerto by Zbigniew Bujarski (1980), the Sonata by Roman Maciejewski (1998), and the Concerto for Violin and Orchestra by Włodzimierz Kotoński (2000).

In 1982, at the time of martial law, Wanda Wiłkomirska emigrated and settled in the Federal Republic of Germany. In 1983 she received a professorship at the State University of Music Heidelberg-Mannheim. Since then, teaching has been a great passion of the violinist.

Wanda Wiłkomirska is a frequent jury member at violin competitions, including those in Moscow, Tokyo, London, Munich, Vienna, Graz, Hanover, Gorizia, Lichtenberg, Poznan, Lodz and Lublin.

After the artist retired, still full of vital and musical energy, she accepted a teaching position at the Sydney Conservatorium of Music. She frequently gives master classes in Poland, Japan, Switzerland, Italy, Finland, Australia, Austria and Germany.

For her artistic activity she was awarded the State Prize 1st and 2nd class, the Commander's Cross of the Order of Poland's Rebirth and the Commander's Cross with Star. In addition, the Australian Overseas Poles awarded the violinist a medal and the Karol Szymanowski Foundation an award.
