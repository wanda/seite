+++
title = 'Wer ist Wanda Wilkomirska'
slug = 'wer-ist-wanda'
image = 'images/wanda.jpg'
description = 'Diese Seite ist der großen polnischen Geigerin Wanda Wiłkomirska gewidmet. Ihre Musikkarriere führte gradlinig von Erfolg zu Erfolg, bis auf den Olymp der weltbesten Interpreten. Ihr Privatleben dagegen, reich an politischen Verstrickungen und Befreiungsschlägen'
disableComments = true
+++
Die große polnische Geigerin Wanda Wiłkomirska, die im Alter von 89 Jahren verstorben ist, wurde besonders mit dem Repertoire ihrer Landsleute Karol Szymanowski, Krzysztof Penderecki - der 1967 sein Capriccio für sie schrieb - und Grażyna Bacewicz in Verbindung gebracht. Sie war auch eine Verfechterin anderer Repertoires des 20. Jahrhunderts, insbesondere von Béla Bartók und Paul Hindemith, und fühlte sich mit den britischen Komponisten Frederick Delius, den sie als einen englischen Szymanowski betrachtete, der seine drei Violinsonaten aufnahm, und Benjamin Britten verbunden.

Wir laden sie auf eine Reise durch das Leben von Wanda ein. Schwerpunkte sind:

- Kindheit
- Studentin
- Zweiten Weltkrieg
- Carnergie Hall
- Karriere
- Ehemann
- Opposition 1976

W. Wiłkomirska ist Preisträgerin der Violinwettbewerbe in Genf (1946), Budapest (1949), Leipzig (1950) und Posen (1952). Die erfolgreiche Teilnahme an diesen Wettbewerben sowie Auftritte mit der Warschauer Philharmonie machten die Künstlerin in Polen und im Ausland bekannt. Von besonderer Bedeutung für ihre Karriere war die musikalische Verbindung zu dem Dirigenten Witold Rowicki, der die Warschauer Nationalphilharmonie entscheidend prägte. Im Jahre 1955 wurde Wanda Wiłkomirska zur Solistin des Orchesters ernannt. Zusammen mit Rowicki konzertierte sie auf vielen Bühnen der Welt. Einen Wendepunkt in ihrer Karriere stellte der Auftritt in der New Yorker Carnegie Hall dar, wo sie im Jahre 1961 zusammen mit dem Orchester der Nationalphilharmonie das Violinkonzert Nr. 1 von Karol Szymanowski spielte. Sol Hurock, damals der berühmteste Musikimpressario, der so namhafte Geiger wie I. Stern und D. Oistrach betreute, bemerkte die außergewöhnliche Begabung W. Wiłkomirskas und öffnete ihr die Tore zum amerikanischen und kanadischen Musikmarkt. Von nun an konzertierte sie jährlich in Amerika im Rahmen von Violinabenden und Philharmoniekonzerten. Im Jahre 1968 nahm sie eine ständige Zusammenarbeit mit der Plattenfirma Connoisseur Society in New York auf, für die sie 12 Platten einspielte, von denen zwei die Auszeichnung „Best of the Year“ (1972) und den „Grand Prix du Disque“(1974) erhielten. Seit jener Zeit arbeitet die Geigerin mit den größten Plattenfirmen wie Deutsche Grammophon, EMI, Philips, Naxos und Polskie Nagrania zusammen.

Im Jahre 1969 errang W. Wiłkomirska große künstlerische Erfolge in Australien. 1973 trat sie als erste Solistin in der Sydney Opera auf. Mit ihrem Spiel gewann sie große Anerkennung; die Geigerin wurde zu zahlreichen Violinabenden und Konzerten mit australischen Symphonieorchestern eingeladen. Seit Anfang der sechziger Jahre konzertiert sie in den bedeutendsten Konzertsälen der Welt, z.B. Carnegie Hall, Lincoln Center New York, Salle Pleyel in Paris, Gewandhaus zu Leipzig, Royal Festival Hall, Großer Saal des Tschaikowski-Konservatoriums Moskau, Berliner Philharmonie und Sydney Opera. Wanda Wilkomirska trat u. a. mit folgenden Orchestern auf: New York Philharmonic, Cleveland Orchestra, Halle Orchester, Royal Philharmonic, Sydney Symphony, Gewandhausorchester Leipzig, Scottish Chamber Orchestra, Royal Concertgebouw Orchester, Berliner Philharmoniker. Sie spielte unter der Leitung so berühmter Dirigenten wie Paul Klecki, Pierre Boulez, Paul Hindemith, Otto Klemperer, Zubin Mehta, Sir John Barbirolli, Wolfgang Sawallisch, Kurt Masur und Erich Leinsdorf. Unter der Leitung von Leinsdorf eröffnete sie 1976 mit einer Aufführung des Violinkonzerts von B. Britten die weltberühmte Barbican Hall in London. 1986, nachdem die institutionelle Apartheid in Südafrika abgeschafft worden war, nahm sie erstmals eine Einladung zu dortigen Konzerten an. Seitdem kann man sagen, Wanda Wilkomirska sei auf allen fünf Kontinenten bekannt. Trotz dieser Erfolge führte sie ihre Zusammenarbeit mit kleineren polnischen Orchestern und Philharmonien fort, wodurch sie immer wieder neue Fans unter den Musikliebhabern und in Musikerkreisen gewinnen konnte.

Die Kammermusik nimmt einen sehr wichtigen Platz in der musikalischen Tätigkeit der Geigerin ein, die in jungen Jahren mit ihrer Schwester Maria am Klavier und ihrem Bruder Kazimierz am Violoncello als "Trio Wilkomirski" auftrat. Als Kammermusikerin arbeitete sie auch mit so bekannten Musikern wie Krystian Zimerman, Gidon Kremer, Martha Argerich, Kim Kashkashin und  Mischa Maisky zusammen. Sie trat u. a. bei den Festivals „Bravo Maestro“, "Gidon Kremer & Friends" in Kumho und "Martha Argerich & Friends" in Bochum auf. Mehrfach wurde die Künstlerin zur Teilnahme an Musikfestivals in Edinburg, Wien, Salzburg, Paris und Warschau eingeladen.

In ihrer Karriere spielen Uraufführungen polnischer zeitgenössischer Musikwerke eine große Rolle. Zu den wichtigsten Uraufführungen gehören Werke wie das Violinkonzert Nr. 5 (1951) und das Violinkonzert Nr. 7 (1979) von Grażyna Bacewicz, die Espressioni Varianti von Tadeusz Baird (1959), Dialoghi per violino e orchestra von Augustyn Bloch (1966), das Capriccio von Krzysztof Penderecki (1968), das Violinkonzert von Zbigniew Bargielski (1977), das Violinkonzert von Zbigniew Bujarski (1980), die Sonate von Roman Maciejewski (1998) und das Konzert für Violine und Orchester von Włodzimierz Kotoński (2000).

Im Jahre 1982, zur Zeit des Kriegsrechts, emigrierte Wanda Wiłkomirska und ließ sich in der Bundesrepublik Deutschland nieder. Im Jahre 1983 erhielt sie eine Professur an der Staatlichen Hochschule für Musik Heidelberg-Mannheim. Seitdem ist das Unterrichten eine große Leidenschaft der Geigerin.

Wanda Wiłkomirska ist häufig Jurymitglied bei Violinwettbewerben, u. a. in Moskau, Tokyo, London, München, Wien, Graz, Hannover, Gorizia, Lichtenberg, Posen, Lodz und Lublin.

Nachdem die Künstlerin in den Ruhestand getreten war, nahm sie, immer noch voller vitaler und musikalischer Kräfte, einen Lehrauftrag am Sydney Conservatorium of Music an. Sie gibt häufig Meisterkurse in Polen, Japan, der Schweiz, Italien, Finnland, Australien, Österreich und Deutschland.

Für ihre künstlerische Tätigkeit wurde sie mit dem Staatspreis 1. und 2. Klasse, dem Kommandeurskreuz des Ordens der Wiedergeburt Polens und dem Kommandeurskreuz mit Stern ausgezeichnet. Außerdem verliehen die australischen Auslandpolen der Geigerin einen Orden  und die Karol Szymanowski-Stiftung eine Auszeichnung.
